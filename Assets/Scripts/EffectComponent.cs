using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EffectComponent : MonoBehaviour
{
  public abstract void ItemEffect(Action callback);
}
