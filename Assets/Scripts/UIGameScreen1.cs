using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIGameScreen1 : MonoBehaviour
{
  [SerializeField] private GameObject _uiItemprefab;
  [SerializeField] private Transform _contentTransform;

  private Dictionary<string, UIGameItem> _uiItems = new Dictionary<string, UIGameItem>();

  public void Init(Dictionary<string, GameItemData> itemsData)
  {
    foreach (var key in _uiItems.Keys)
    {
      Destroy(_uiItems[key].gameObject);
    }
    _uiItems.Clear();
    GenerateUIItems(itemsData);
  }

  private void GenerateUIItems(Dictionary<string, GameItemData> itemsData)
  {
    foreach (var key in itemsData.Keys)
    {
      GameObject uiGameItem = Instantiate(_uiItemprefab, _contentTransform);
      UIGameItem gameItemcomponent = uiGameItem.GetComponent<UIGameItem>();

      gameItemcomponent.SetSprite(itemsData[key].Sprite);
      gameItemcomponent.SetCount(itemsData[key].Count);

      _uiItems.Add(key, gameItemcomponent);
    }
  }

  public void OnItemsFind(string name)
  {
    _uiItems[name].Decrease();
    if (_uiItems[name].Count == 0)
    {
      Destroy(_uiItems[name].gameObject);
      _uiItems.Remove(name);
    }
  }
}
