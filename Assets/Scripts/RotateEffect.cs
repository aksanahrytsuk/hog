using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateEffect : EffectComponent
{
  public override void ItemEffect(Action callback) => transform.DORotate(new Vector3(0f, 0f, 360f), 0f, RotateMode.WorldAxisAdd).OnComplete(() =>
  {
      gameObject.SetActive(false);
      callback?.Invoke();
  });
}
