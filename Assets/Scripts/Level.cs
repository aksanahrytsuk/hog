using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
  List<ClickableItems> _items = new List<ClickableItems>();
  int _itemsCount;

  public event Action OnComplete;
  public event Action<string> OnItemListChange;

  public void Initialize()
  {
    // Поиск сомпонентов clickableItems в Level возвращает массив всех найденых объектов
    ClickableItems[] clickableItems = GetComponentsInChildren<ClickableItems>();
    // массив clickableItems в list 
    _items.AddRange(clickableItems);
    //счетчик равен количеству в листе
    _itemsCount = _items.Count;

    for (int i = 0; i < _items.Count; i++)
    {
      _items[i].onFind += Found;
    }
  }

  //метод создает словарь с информацией для скрол вью интерфейса
  public Dictionary<string, GameItemData> GetItemDictionary()
  {
    Dictionary<string, GameItemData> dictionary = new Dictionary<string, GameItemData>();

    for (int i = 0; i < _items.Count; i++)
    {
      if (dictionary.ContainsKey(_items[i].Name))
      {
        //обращение по ключу в dictionary поолучили объект ameItemData получаем IncreaseCount() 
        dictionary[_items[i].Name].IncreaseCount();
      }
      else
      {
        dictionary.Add(_items[i].Name, new GameItemData(_items[i].Sprite)); //
      }
    }
    return dictionary;
  }

  private void Found(string name)
  {
    _itemsCount--;
    OnItemListChange?.Invoke(name); //
    if (_itemsCount == 0)
    {
      OnComplete?.Invoke();
      Debug.Log("You complete!");
    }
  }
}
