using System;
using UnityEngine;
using DG.Tweening;

public class DisappearEffect : EffectComponent
{
  [SerializeField] private float _disappearDuration;
  SpriteRenderer _spriteRend;
  public override void ItemEffect(Action callback)
  {
    _spriteRend.DOFade(0f, _disappearDuration).OnComplete(() =>
    {
        gameObject.SetActive(false);
        //вызов 
        callback?.Invoke();
    });
  }
}
