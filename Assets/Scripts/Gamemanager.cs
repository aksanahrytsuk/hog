using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Gamemanager : MonoBehaviour
{
  [SerializeField] UIManager _uiManager;
  [SerializeField] private List<Level> _levels = new List<Level>();
  //для сохранения текущего уровня,  покф это не обходимо
  private Level _currentLevel;

  public UnityEvent<string> ChangeItemList;

  //номер уровня на котором находится игрок(может быть болоьше, чем количество уровней)
  private int _levelNumber;

  void Start()
  {
    _uiManager.ShowStartScreen();
  }

  private void CreateLevel()
  {
    //если есть старый уровень, удаляем
    if (_currentLevel != null)
    {
      Destroy(_currentLevel.gameObject);
      _currentLevel = null;
    }
    //
    int index = _levelNumber;
    if (_levelNumber >= _levels.Count)
    {
      index = _levelNumber % _levels.Count; // индекс текущего уровня исходя из количества уровней
    }
    //создаем новый уровень
    _currentLevel = Instantiate(_levels[index].gameObject).GetComponent<Level>();
  }
  public void StartGame()
  {
    CreateLevel();
    //если найдены все вещи, вызов метода
    _currentLevel.OnComplete += StopGame;
    _currentLevel.OnItemListChange += OnItemListChange;
;
    _currentLevel.Initialize();
    
    //генерация данныхю между левелом и интерфейсом
    _uiManager.ShowGameScreen(_currentLevel.GetItemDictionary());
  }

  private void StopGame()
  {
    _uiManager.ShowWinScreen();
    //увеличиваем индекс уровня и создаём новый уровень
    _levelNumber++;
  }

  public void ExitGame()
  {
    Application.Quit();
  }

  private void OnItemListChange(string name) => ChangeItemList?.Invoke(name);
}
