using System;
using UnityEngine;

//добавляет автоматически компонент SpriteRenderer
[RequireComponent(typeof(SpriteRenderer))]
public class ClickableItems : MonoBehaviour
{
  // для индетнификации объекта
  [SerializeField] private string _name;
  
  private EffectComponent _effectComponent;
  private SpriteRenderer _spriteRenderer; 
  public Sprite Sprite => _spriteRenderer.sprite;
  public string Name => _name;
  

  //ивент создан для подписчиков
  public event Action<string> onFind;
  void Awake()
  {
    // для каждого объекта вернет свой EffectComponent
    _effectComponent = GetComponent<EffectComponent>();
    //компонент priteRenderer объекта (НЕ image canvas)
    _spriteRenderer = GetComponent<SpriteRenderer>();
  }

  public void OnMouseUpAsButton()
  {
    OnFindedMethod();
  }

  public void OnFindedMethod()
  {
    //для каждого объекта свой эффект 
    _effectComponent.ItemEffect(() =>
    {
      // вызов ивента after callback event
      onFind?.Invoke(_name);
      onFind = null;
      gameObject.SetActive(false);
    });
  }
}
