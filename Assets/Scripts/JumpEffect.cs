using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class JumpEffect : EffectComponent
{
  [SerializeField] private float _jumpPower;
  [SerializeField] private int _numJumps;
  [SerializeField] private float _duration;

  public override void ItemEffect(Action callback) => transform.DOJump(new Vector3(10f, 0f, 0f), 3, 10, 3, false).OnComplete(() =>
   {
     gameObject.SetActive(false);
     callback?.Invoke();
   });
}
